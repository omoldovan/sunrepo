package net.java.dev.netbeansspot;

import com.sun.spot.resources.transducers.ITemperatureInput;
import com.sun.spot.service.Task;
import com.sun.spot.service.TaskManager;
import java.io.IOException;

/**
 * The TempControll class implements the methods needed to controll and monitor
 * a specific temperature sensor. It takes samples and prints them each 5
 * seconds along with an "last minute" average.
 * 
* @author Ovidiu Mircea Moldovan
 * @version 1.0
 * @since 2015-12-08
 */
public class TempControll {

    private ITemperatureInput ourTempSensor;
    private int temp_sleep_time, minute_division;
    private Task tempTask;
    private double medTemp, tmpTemp, acumulator;
    private int samplerCounter;

    /**
     * Constructor. It recieves an ITemperatureInput and a check inverval.
     *
     * @param temp ITemperatureInput This is an object correponding to a
     * temerature sensor.
     * @param sleep_time int This is an integer valuer for the check interval
     */
    public TempControll(ITemperatureInput temp, int sleep_time) {
        ourTempSensor = temp;
        temp_sleep_time = sleep_time;
        minute_division = 60 / sleep_time;

        medTemp = 0;
        tmpTemp = 0;
        acumulator = 0;
        samplerCounter = 1;
    }

    /**
     * This method creates and starts a new Task or thread to monitor the
     * temperature sensor. It reads the state each 1000 * "temp_sleep_time"
     * milliseconds. Each 5 seconds prints the value of the current temperature
     * and the average of the las minute.
     */
    public void tempTask() {
        // start periodically reporting the temperature
// define the task - report temperature every 5 seconds

        tempTask = new Task(temp_sleep_time * 1000) {
            public void doTask() {
                try {
                    //get the current temperature
                    tmpTemp = ourTempSensor.getCelsius();
                    // print the current temperature
                    System.out.println("Current temperature is " + tmpTemp);
                    System.out.println("Average temperature is " + medTemp);
                    //if there weren't 12 reads then sum the currente temperature
                    //else print the average temperature

                    if (samplerCounter <= minute_division) {
                        acumulator = acumulator + tmpTemp;
                    } else {

                        medTemp = acumulator / minute_division;

                        System.out.println("The new average temperature is: " + medTemp);
                        //reset counters
                        tmpTemp = 0;
                        acumulator = 0;
                        samplerCounter = 0;
                    }
                    //count another temperature read
                    samplerCounter++;
                } catch (IOException ex) {
                    ex.printStackTrace();
                    System.out.println("Error during temperature read");
                }
            }
        };
        tempTask.start();
    }

    /**
     * This method stops the thread started by this class.
     */
    public void tempStop() {
        tempTask.stop();
        TaskManager.unschedule(tempTask);
    }

    /**
     * This method is used to reset all the read thread counters.
     */
    public void reset_temp_counters() {

        System.out.println("Reseting temperature task counters");
        //reset counters
        medTemp = 0;
        tmpTemp = 0;
        acumulator = 0;
        samplerCounter = 1;
    }
}
