package net.java.dev.netbeansspot;

import com.sun.spot.resources.transducers.IAccelerometer3D;
import com.sun.spot.service.Task;
import com.sun.spot.service.TaskManager;
import java.io.IOException;

/**
 * The AccelerometerControll class implements the methods needed to controll and
 * monitor a specific accelerometer sensor. If it detects a movment then lights
 * all the leds to red.
 * 
* @author Ovidiu Mircea Moldovan
 * @version 1.0
 * @since 2015-12-08
 */
public class AccelerometerControll {

    private IAccelerometer3D accelerometer;
    private Task movementTask;
    private int acce_sleep_time;
    private LedControll myLeds;
    private double accelInitial;

    /**
     * Constructor. It recieves an IAccelerometer3D object and a check inverval.
     *
     * @param acc IAccelerometer3D This is a IAccelerometer3D correponding to a
     * acceleormeter sensor.
     * @param sleep_time int This is an integer valuer for the check interval
     */
    public AccelerometerControll(IAccelerometer3D acc, int sleep_time,
            LedControll leds) {
        accelerometer = acc;
        acce_sleep_time = sleep_time;
        myLeds = leds;
    }

    /**
     * This method is used to check the acceleration sensor. Returns the sensor
     * read.
     *
     * @return double This returns the sensor read.
     */
    public double getAcceleration() {
        try {
            return accelerometer.getAccel();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Error during acceleartion read");
        }
        return 0;
    }

    /**
     * This method creates and starts a new Task or thread to monitor the
     * acceleration sensor. It reads the state each 500 * "acce_sleep_time"
     * milliseconds. If the new value is diferent from the first one then sets
     * all the leds to red.
     *
     * @param accel This is a double value that is use as an reference
     */
    public void movementTask(double accel) {
    // start periodically reporting the acceleration
        // define the task - report position changed every 0.1 seconds
        accelInitial = accel;
        movementTask = new Task(acce_sleep_time * 500) {
            public void doTask() {
                try {

                    if (accelerometer.getAccel() != accelInitial) {
                        myLeds.ledsOn(LedControll.RED);
                    }

                } catch (IOException ex) {
                    ex.printStackTrace();
                    System.out.println("Error during task of acceleartion read");
                }
            }
        };

        movementTask.start();
    }

    /**
     * This method stops the thread started by this class.
     */
    public void movementStop() {
        movementTask.stop();
        TaskManager.unschedule(movementTask);
    }
}
