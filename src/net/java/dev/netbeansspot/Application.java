package net.java.dev.netbeansspot;

import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.*;
import com.sun.spot.service.Task;
import java.io.IOException;

/**
 * The Application program implements an application that simply obtains data
 * from diferente sensors for a Sun Spot and actas over the leds and writes the
 * results to the standard output.
 * 
* @author Ovidiu Mircea Moldovan
 * @version 1.0
 * @since 2015-12-08
 */
public class Application extends MIDlet implements ISwitchListener {

    //STATIC vars
    public static final int TEMP_SLEEP_TIME = 5;
    public static final int ACCE_SLEEP_TIME = 1;

    //Controllers definition
    LedControll leds;
    AccelerometerControll acc;
    TempControll temp;

    //sensors definition
    ITemperatureInput ourTempSensor;
    ITriColorLEDArray ourLEDs;
    IAccelerometer3D accelerometer1;
    ISwitch sw1, sw2;

    /**
     * This method is used to start the app. It initialize the controllers
     * classes with the proper
     */
    protected void startApp() throws MIDletStateChangeException {
        System.out.println("****Starting spot monitoring....*********");
        //*******SENSORS INITIALIZATION*******************//
        ourTempSensor
                = (ITemperatureInput) Resources.lookup(ITemperatureInput.class);
        ourLEDs
                = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
        accelerometer1
                = (IAccelerometer3D) Resources.lookup(IAccelerometer3D.class);
        sw1 = (ISwitch) Resources.lookup(ISwitch.class, "SW1");
        sw2 = (ISwitch) Resources.lookup(ISwitch.class, "SW2");

        //*******Controllers INITIALIZATION*******************//
        leds = new LedControll(ourLEDs);
        acc = new AccelerometerControll(accelerometer1, ACCE_SLEEP_TIME, leds);
        temp = new TempControll(ourTempSensor, TEMP_SLEEP_TIME);

        //start switch listeners
        sw1.addISwitchListener(this);
        sw2.addISwitchListener(this);

        //start the temperature task
        temp.tempTask();

        //start movement task
        acc.movementTask(acc.getAcceleration());

        //start the analog check routine
        while (true) {
            try {
                if (all_analog_off()) {
                    leds.ledsOn(LedControll.GREEN);

                    Thread.sleep(500);

                } else {
                    Thread.sleep(500);
                    leds.ledsOff();
                }
            } catch (InterruptedException interruptedException) {

            }

        }
    }

    /**
     * This method is used to check if all analogs entries are off. Checks them
     * one by one.
     *
     * @return boolean This returns true if all analgos are off.
     */
    private boolean all_analog_off() {
        try {
            //check all the 6 analog entries
            for (int i = 0; i < 6; i++) {
                String analogName = "A" + i;
                IAnalogInput analogIn = (IAnalogInput) Resources.lookup(IAnalogInput.class, analogName);

                if (analogIn.getVoltage() != 0) {
                    //if any of the entries are on then return false
                    return false;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("Error during analog entries read");
        }
        return true;

    }

    /**
     * This method overrides the switchPressed method of inherit class. It
     * resets the timer class counters if the switch is pressed.
     *
     * @param evt This is a SwitchEvent object with caller info
     */
    public void switchPressed(SwitchEvent evt) {
        System.out.println("Switch pressed");
        temp.reset_temp_counters();
    }

    /**
     * This method overrides the switchReleased method of inherit class. It's
     * implementation is needed for parent/inherit class.
     *
     * @param evt This is a SwitchEvent object with caller info
     */
    public void switchReleased(SwitchEvent evt) {
        System.out.println("Switch released");
    }

    /**
     * This is a specific Sun Spot management method Not called by the Squawk
     * VM.
     */
    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system. I.e. if startApp throws
     * any exception other than MIDletStateChangeException, if the isolate
     * running the MIDlet is killed with Isolate.exit(), or if VM.stopVM() is
     * called.
     *
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true when this method is called, the MIDlet must
     * cleanup and release all resources. If false the MIDlet may throw
     * MIDletStateChangeException to indicate it does not want to be destroyed
     * at this time.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
        // TODO
        System.out.println("****Shuting down....*********");

        leds.ledsOff();
        acc.movementStop();
        temp.tempStop();

    }
}
