package net.java.dev.netbeansspot;

import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ITriColorLEDArray;

/**
 * The LedControll class implements the methods needed in order to control an
 * specific ITriColorLEDArray .
 * 
* @author Ovidiu Mircea Moldovan
 * @version 1.0
 * @since 2015-12-08
 */
public class LedControll {

    /**
     *
     */
    public static final int RED = 1;

    /**
     *
     */
    public static final int GREEN = 2;

    /**
     *
     */
    public static final int BLUE = 3;
    ITriColorLEDArray ourLEDs;

    /**
     * Constructor
     * @param leds
     */
    public LedControll(ITriColorLEDArray leds) {
        ourLEDs = leds;
    }

    /**
     * This method turns off all the leds of the array.
     */
    public void ledsOff() {
        ourLEDs.setOff();
    }

    /**
     * This method sets all the leds of the array to a color. It takes a color
     * code, marked as static, and use it to set that color to all the leds.
     *
     * @param color This is color code (int) from the static vars of the class
     */
    public void ledsOn(int color) {
        /*Colors are specified with the setRGB(int red, int green, int blue) method. Individual LEDs
         are referenced with getLED(int index).
         // set the LED color desired*/
// set the first two LEDs to bright red, the next two to bright green,
// the next two to bright blue, and the last two to white.
// First two = bright red
        for (int i = 0; i < 8; i++) {
            switch (color) {
                case RED:
                    ourLEDs.getLED(i).setRGB(255, 0, 0);
                    break;
                case GREEN:
                    ourLEDs.getLED(i).setRGB(0, 255, 0);
                    break;
                case BLUE:
                    ourLEDs.getLED(i).setRGB(0, 0, 255);
                    break;
                default:
                    System.out.println("The leds can't be set to color code" + color);
                    break;
            }
        }
        ourLEDs.setOn();
    }
}
